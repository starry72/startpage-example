# Startpage example

![An example startpage](https://gitlab.com/vallode/startpage-example/-/raw/master/preview.png)

A small, minimal startpage created for [stpg.tk](https://stpg.tk/). It includes
a small image alongside some quick links.